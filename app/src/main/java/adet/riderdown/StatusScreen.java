package adet.riderdown;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class StatusScreen extends AppCompatActivity {

    private final int REQUEST_LOCATION = 0xf0a0;
    private final int REQUEST_CALL = 0xfa31;
    private final int REQUEST_SMS = 0xf122;

    private final BroadcastReceiver stateRecv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            readAndRefreshState(intent);
        }
    };
    private final BroadcastReceiver permissionsRecv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ((ActivityCompat.checkSelfPermission(StatusScreen.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(StatusScreen.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))
            {
                Toast.makeText(StatusScreen.this, "Locations permissions are required to continue.", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(StatusScreen.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                return;
            }
            if (ActivityCompat.checkSelfPermission(StatusScreen.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(StatusScreen.this, "Call permissions are required to continue.", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(StatusScreen.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
                return;
            }
            if (ActivityCompat.checkSelfPermission(StatusScreen.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(StatusScreen.this, "SMS permissions are required to continue.", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(StatusScreen.this, new String[]{Manifest.permission.SEND_SMS}, REQUEST_SMS);
            }
        }
    };
    private final BroadcastReceiver smsSentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode())
            {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
    private final BroadcastReceiver smsDeliveredReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            switch (getResultCode())
            {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private LocalBroadcastManager localBroadcast;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch(requestCode) {
            case REQUEST_LOCATION:
            case REQUEST_CALL:
            case REQUEST_SMS:
                startService(new Intent(this, DetectorService.class));
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        localBroadcast.unregisterReceiver(stateRecv);
        localBroadcast.unregisterReceiver(permissionsRecv);
        localBroadcast.unregisterReceiver(smsDeliveredReceiver);
        localBroadcast.unregisterReceiver(smsSentReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_screen);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        startService(new Intent(this, DetectorService.class));
        Intent toSend = new Intent(EmergencyResponses.RECEIVE_EMERGENCY_NOTICE);
        toSend.putExtra(EmergencyResponses.CancelEmergency, true);
        LocalBroadcastManager.getInstance(StatusScreen.this).sendBroadcast(toSend);

        localBroadcast = LocalBroadcastManager.getInstance(this);
        localBroadcast.registerReceiver(stateRecv, new IntentFilter(DetectorService.STATE_UPDATE_STRING));
        localBroadcast.registerReceiver(permissionsRecv, new IntentFilter(DetectorService.PERMISSIONS_NEEDED_TO_RUN_SERVICE));
        localBroadcast.registerReceiver(smsDeliveredReceiver, new IntentFilter(CommunicationsService.SMS_DELIVERED_STRING));
        localBroadcast.registerReceiver(smsSentReceiver, new IntentFilter(CommunicationsService.SMS_SENT_STRING));

        Button b = (Button) findViewById(R.id.configureButton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StatusScreen.this, ConfigureScreen.class));
            }
        });

        b = (Button) findViewById(R.id.stopEmergency);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            //This routes via DetectorService and will issue an update to the Activity from the Service aka Activity -> Service -> Activity
            public void onClick(View v) {
                Intent toSend = new Intent(EmergencyResponses.RECEIVE_EMERGENCY_NOTICE);
                toSend.putExtra(EmergencyResponses.CancelEmergency, true);
                LocalBroadcastManager.getInstance(StatusScreen.this).sendBroadcast(toSend);
            }
        });
    }

    private void readAndRefreshState(Intent intent) {
        final Drawable notmoving = getResources().getDrawable(getResources().getIdentifier("@drawable/notmoving", null, getPackageName()));
        final Drawable moving = getResources().getDrawable(getResources().getIdentifier("@drawable/moving", null, getPackageName()));
        final Drawable emergency = getResources().getDrawable(getResources().getIdentifier("@drawable/emergency", null, getPackageName()));
        ImageView button = (ImageView) findViewById(R.id.imageButton);
        Button cancelAcc = (Button) findViewById(R.id.stopEmergency);
        button.setClickable(false);

        if (intent.getFloatExtra(TravellingService.TravellingKeyString, 0.0f) >= TravellingService.tenKsPerHour) {
            if (intent.getFloatExtra(ShockDetectService.AccelerationKeyString, 0.0f) > 0.0f) {
                button.setImageDrawable(emergency);
                cancelAcc.setEnabled(true);
                cancelAcc.setBackgroundColor(Color.argb(0x99,0x0e,0x14,0x2a));
            } else {
                cancelAcc.setEnabled(false);
                button.setImageDrawable(moving);
                cancelAcc.setBackgroundColor(Color.argb(0x99,0x25,0x25,0x25));
            }
        } else {
            cancelAcc.setEnabled(false);
            button.setImageDrawable(notmoving);
            cancelAcc.setBackgroundColor(Color.argb(0x99,0x25,0x25,0x25));
        }
    }
}
