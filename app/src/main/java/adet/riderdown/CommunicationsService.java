package adet.riderdown;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.List;

import static adet.riderdown.ApplicationConstants.LOG_TAG;

/**
 * Created by lordnyson on 1/11/2017.
 */

public class CommunicationsService {
    public final static String SMS_DELIVERED_STRING = "SMS_DELIVERED";
    public final static String SMS_SENT_STRING = "SMS_SENT";
    private final static String simSlotName[] = {
            "extra_asus_dial_use_dualsim",
            "com.android.phone.extra.slot",
            "slot",
            "simslot",
            "sim_slot",
            "subscription",
            "Subscription",
            "phone",
            "com.android.phone.DialingMode",
            "simSlot",
            "slot_id",
            "simId",
            "simnum",
            "phone_type",
            "slotId",
            "slotIdx"
    };

    private Service parent;

    public CommunicationsService(Service parent) {
        this.parent = parent;
    }


    @TargetApi(23)
    private void chooseSim(Intent intent) {
        TelecomManager telecomManager = (TelecomManager) parent.getSystemService(Context.TELECOM_SERVICE);
        if (ActivityCompat.checkSelfPermission(parent, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        List<PhoneAccountHandle> phoneAccountHandleList = telecomManager.getCallCapablePhoneAccounts();
        intent.putExtra("com.android.phone.force.slot", true);
        intent.putExtra("Cdma_Supp", true);
        //Add all slots here, according to device.. (different device require different key so put all together)
        for (String s : simSlotName)
            intent.putExtra(s, 0);

        if (phoneAccountHandleList != null && phoneAccountHandleList.size() > 0)
            intent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandleList.get(0));
    }

    public void callNumber(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            Uri uri = Uri.parse("tel:" + number);
            callIntent.setData(uri);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                callIntent.setPackage("com.android.server.telecom");
                chooseSim(callIntent);
            } else {
                callIntent.setPackage("com.android.phone");
            }
            if (ActivityCompat.checkSelfPermission(parent, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            parent.startActivity(callIntent);
        } catch (Exception e)
        {
            Log.e(LOG_TAG, e.getMessage());
        }
    }
    public void setAcceptAllCalls(boolean acceptAllCalls) {
        try {
            Intent intent = new Intent(parent, AcceptCallActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            parent.startActivity(intent);
        } catch (Exception e)
        {
            Log.e(LOG_TAG, e.getMessage());
        }
    }
    public void sendTextMessage(String number, String message)
    {
        try {
            if (ActivityCompat.checkSelfPermission(parent, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            SmsManager sms = SmsManager.getDefault();
            PendingIntent sentPI = PendingIntent.getBroadcast(parent, 0, new Intent(SMS_SENT_STRING), 0);
            PendingIntent deliveredPI = PendingIntent.getBroadcast(parent, 0, new Intent(SMS_DELIVERED_STRING), 0);
            sms.sendTextMessage(number, null, message, sentPI, deliveredPI);
        } catch (Exception e)
        {
            Log.e(LOG_TAG, e.getMessage());
        }
    }
}
