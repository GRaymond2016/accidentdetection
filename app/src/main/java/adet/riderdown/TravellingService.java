package adet.riderdown;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Iterator;
import java.util.TreeMap;

public class TravellingService implements android.location.LocationListener {
    public static final String RECEIVE_TRAVELLING_NOTIFICATION = "detectorservice.travellingnotification";
    public static final String TravellingKeyString = "travelling";
    public static final double tenKsPerHour = 0.0;//2.8;

    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private static final double fiveMinsInNanos = 3e+11;

    private Location location = null;
    private LocalBroadcastManager localBroadcast;
    private final TreeMap<Long, Float> speeds = new TreeMap<Long, Float>();

    public TravellingService(LocalBroadcastManager localBroadcast) {
        this.localBroadcast = localBroadcast;
    }

    public Location getLastLocation()
    {
        return location;
    }

    public void onLocationChanged(final Location loc) {
        if (isBetterLocation(loc, location)) {
            location = loc;
        }
        if (loc.hasSpeed() && loc.getSpeed() >= tenKsPerHour) {
            speeds.put(loc.getElapsedRealtimeNanos(), loc.getSpeed());
        }

        int count = 0;
        float total = 0;
        for (Iterator<TreeMap.Entry<Long, Float>> iter = speeds.entrySet().iterator(); iter.hasNext(); ) {
            TreeMap.Entry<Long, Float> entry = iter.next();
            if (entry.getKey() < SystemClock.elapsedRealtimeNanos() - fiveMinsInNanos) {
                iter.remove();
            }
            if (entry.getValue() > 0.01) {
                total += entry.getValue();
                count += 1;
            }
        }
        if (count > 0) {
            total /= count;
        }
        Intent intent = new Intent(RECEIVE_TRAVELLING_NOTIFICATION);
        intent.putExtra(TravellingKeyString, total);
        localBroadcast.sendBroadcast(intent);
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    //Taken from android examples

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
