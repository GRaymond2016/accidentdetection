package adet.riderdown;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

class EmergencyResponses {
    static final String RECEIVE_EMERGENCY_NOTICE = "emergencyresponses.emergencynotice";
    static final String CancelEmergency = "cancelEmergency";

    private Calendar lastTime = null;

    private ConfigurationModel model;
    private LocalBroadcastManager localBroadcast;

    private TravellingService locService;
    private CommunicationsService callService;

    private Timer emergencyTimer = null;

    private final BroadcastReceiver config_recv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConfigurationCoding.decodeConfiguration(model, intent);
        }
    };

    private final BroadcastReceiver emergency_recv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean cancel = intent.getBooleanExtra(CancelEmergency, false);
            if (cancel) {
                if (emergencyTimer != null) {
                    emergencyTimer.cancel();
                }
                emergencyTimer = null;
            } else {
                if (lastTime != null && lastTime.compareTo(Calendar.getInstance()) > 0)
                    return;
                lastTime = Calendar.getInstance();
                lastTime.add(Calendar.SECOND, model.timeBeforeCall);
                if (emergencyTimer == null) {
                    emergencyTimer = new Timer();
                    emergencyTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            emergency();
                            emergencyTimer = null;
                        }
                    }, model.timeBeforeCall * 1000);
                }
            }
        }
    };

    EmergencyResponses(LocalBroadcastManager localBroadcast, ConfigurationModel model, TravellingService locationService, CommunicationsService callService)
    {
        this.model = model;
        this.localBroadcast = localBroadcast;
        this.locService = locationService;
        this.callService = callService;

        this.localBroadcast.registerReceiver(config_recv, new IntentFilter(ConfigurationCoding.RECEIVE_CONFIGURATION_OPTIONS));
        this.localBroadcast.registerReceiver(emergency_recv, new IntentFilter(DetectorService.STATE_UPDATE_STRING));
    }

    void sentText()
    {
        try
        {
            Location loc = locService.getLastLocation();
            if (loc != null) {
                callService.sendTextMessage(model.emergencyNumber, "The rider down android app has detected I may have been involved in an accident; you may want to try calling me. My Location is: http://maps.google.com?q=" + loc.getLatitude() + "," + loc.getLongitude() + "/");
            }
        }
        catch (Exception e)
        {

        }
    }
    private void emergency()
    {
        if (model.sendText)
        {
            sentText();
        }
        if (model.callNumber)
        {
            callService.callNumber(model.emergencyNumber);
        }
        callService.setAcceptAllCalls(model.acceptCalls);
    }

    void destroy()
    {
        localBroadcast.unregisterReceiver(config_recv);
        localBroadcast.unregisterReceiver(emergency_recv);
    }
}
