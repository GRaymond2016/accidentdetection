package adet.riderdown;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import static adet.riderdown.ConfigurationCoding.RECEIVE_CONFIGURATION_OPTIONS;

public class ConfigureScreen extends AppCompatActivity {


    private CheckBox sendCall;
    private CheckBox sendSMS;
    private CheckBox acceptCall;

    private TextView accelEdit;
    private TextView timeToCallEdit;
    private TextView phoneEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_screen);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Context ctx = getApplicationContext();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx);
        ConfigurationModel model = new ConfigurationModel();
        ConfigurationCoding.readConfiguration(model, settings);

        sendCall = ((CheckBox) findViewById(R.id.sendCall));
        sendCall.setChecked(model.callNumber);
        sendSMS = ((CheckBox) findViewById(R.id.sendSMS));
        sendSMS.setChecked(model.sendText);
        acceptCall = ((CheckBox) findViewById(R.id.acceptCall));
        acceptCall.setChecked(model.acceptCalls);

        accelEdit = ((TextView) findViewById(R.id.accelEdit));
        accelEdit.setText(model.accelBeforeEmergency.toString());
        timeToCallEdit = ((TextView) findViewById(R.id.timeToCallEdit));
        timeToCallEdit.setText(model.timeBeforeCall.toString());
        phoneEdit = ((TextView) findViewById(R.id.phoneEdit));
        phoneEdit.setText(model.emergencyNumber);

        Button b = (Button) findViewById(R.id.applyButton);
        b.setSelected(true);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfigurationModel model = new ConfigurationModel();
                model.accelBeforeEmergency = Integer.parseInt(accelEdit.getText().toString());
                model.timeBeforeCall = Integer.parseInt(timeToCallEdit.getText().toString());
                model.emergencyNumber = phoneEdit.getText().toString();
                model.acceptCalls = acceptCall.isChecked();
                model.callNumber = sendCall.isChecked();
                model.sendText = sendSMS.isChecked();

                Context ctx = getApplicationContext();
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx);
                ConfigurationCoding.writeConfiguration(model, settings);

                Intent intent = new Intent(RECEIVE_CONFIGURATION_OPTIONS);
                ConfigurationCoding.encodeConfiguration(model, intent);

                LocalBroadcastManager.getInstance(ConfigureScreen.this).sendBroadcast(intent);
                startActivity(new Intent(ConfigureScreen.this, StatusScreen.class));
            }
        });

        Button test = (Button) findViewById(R.id.testButton);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(new Intent(ConfigureScreen.this, DetectorService.class));

                Intent intent = new Intent(DetectorService.TESTING_KEY_STRING);
                LocalBroadcastManager.getInstance(ConfigureScreen.this).sendBroadcast(intent);
                startActivity(new Intent(ConfigureScreen.this, StatusScreen.class));
            }
        });
    }
}
