package adet.riderdown;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;

public class DetectorService extends Service {
    public static String PERMISSIONS_NEEDED_TO_RUN_SERVICE = "detectorservice.permissionsneeded";
    public static String TESTING_KEY_STRING = "detectorservice.runtest";
    public static String STATE_UPDATE_STRING = "detectorservice.stateupdate";

    private LocationManager locationManager;
    private SensorManager sensorManager;
    private LocalBroadcastManager broadcastManager;

    private Sensor accelerometer;
    EmergencyResponses responses;
    CommunicationsService calls;
    private ConfigurationModel model;

    public TravellingService speed;
    public ShockDetectService impulse;

    public DetectorService() {
        calls = new CommunicationsService(this);
    }

    private final BroadcastReceiver cancelRecv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean cancel = intent.getBooleanExtra(EmergencyResponses.CancelEmergency, true);
            if (!cancel)
                return;

            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor edit = settings.edit();
            edit.putBoolean(EmergencyResponses.CancelEmergency, cancel);
            edit.putFloat(ShockDetectService.AccelerationKeyString, 0.0f);
            edit.commit();
            sendStateUpdate(settings);
        }
    };
    private final BroadcastReceiver impulseRecv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            boolean travelling = settings.getFloat(TravellingService.TravellingKeyString, 0.0f) >= TravellingService.tenKsPerHour;
            float shock = intent.getFloatExtra(ShockDetectService.AccelerationKeyString, 0.0f);
            SharedPreferences.Editor edit = settings.edit();
            if (travelling) {
                edit.putBoolean(EmergencyResponses.CancelEmergency, false);
                edit.putFloat(ShockDetectService.AccelerationKeyString, shock);
            }
            edit.commit();
            sendStateUpdate(settings);
        }
    };
    private final BroadcastReceiver locationRecv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            float speed = intent.getFloatExtra(TravellingService.TravellingKeyString, 0.0f);
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor edit = settings.edit();
            edit.putFloat(TravellingService.TravellingKeyString, speed);
            edit.commit();
            sendStateUpdate(settings);
        }
    };
    private final BroadcastReceiver testRecv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ((ActivityCompat.checkSelfPermission(DetectorService.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(DetectorService.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                    (ActivityCompat.checkSelfPermission(DetectorService.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED && model.callNumber) ||
                    (ActivityCompat.checkSelfPermission(DetectorService.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED && model.sendText)) {
                LocalBroadcastManager.getInstance(DetectorService.this).sendBroadcast(new Intent(PERMISSIONS_NEEDED_TO_RUN_SERVICE));
                return;
            }
            if (model.sendText) {
                locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, speed, null);
                Location loc = speed.getLastLocation();
                if (loc != null) {
                    calls.sendTextMessage(model.emergencyNumber, "This is a test SMS from the rider down app. Someone may be trying to set you up as an emergency contact. Their current position is: " + loc.getLatitude() + "," + loc.getLongitude());
                }
            }
            if (model.callNumber) {
                calls.callNumber(model.emergencyNumber);
            }
            calls.setAcceptAllCalls(true);
        }
    };

    @Override
    public void onCreate() {
        model = new ConfigurationModel();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        ConfigurationCoding.readConfiguration(model, settings);

        broadcastManager = LocalBroadcastManager.getInstance(this);
        impulse = new ShockDetectService(broadcastManager, model);
        speed = new TravellingService(broadcastManager);
        responses = new EmergencyResponses(broadcastManager, model, speed, calls);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(impulse, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        StatusNotification.Create(this);

        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED && model.callNumber) ||
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED && model.sendText)) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(PERMISSIONS_NEEDED_TO_RUN_SERVICE));
            stopSelf();
            return START_STICKY;
        }

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 4000, 0, speed);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 4000, 0, speed);
        locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, speed, null);

        LocalBroadcastManager.getInstance(DetectorService.this).registerReceiver(locationRecv, new IntentFilter(TravellingService.RECEIVE_TRAVELLING_NOTIFICATION));
        LocalBroadcastManager.getInstance(DetectorService.this).registerReceiver(impulseRecv, new IntentFilter(ShockDetectService.RECEIVE_ACCELERATION_NOTIFICATION));
        LocalBroadcastManager.getInstance(DetectorService.this).registerReceiver(cancelRecv, new IntentFilter(EmergencyResponses.RECEIVE_EMERGENCY_NOTICE));
        LocalBroadcastManager.getInstance(DetectorService.this).registerReceiver(testRecv, new IntentFilter(TESTING_KEY_STRING));

        StatusNotification.Create(this);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
                locationManager != null) {
            locationManager.removeUpdates(speed);
        }
        sensorManager.unregisterListener(impulse);

        LocalBroadcastManager.getInstance(DetectorService.this).unregisterReceiver(locationRecv);
        LocalBroadcastManager.getInstance(DetectorService.this).unregisterReceiver(impulseRecv);
        LocalBroadcastManager.getInstance(DetectorService.this).unregisterReceiver(cancelRecv);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor edit = settings.edit();
        edit.putFloat(TravellingService.TravellingKeyString, 0.0f);
        edit.putFloat(ShockDetectService.AccelerationKeyString, 0.0f);
        edit.putBoolean(EmergencyResponses.CancelEmergency, true);
        edit.commit();

        responses.destroy();
        impulse.destroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendStateUpdate(SharedPreferences settings) {
        Intent intent = new Intent(STATE_UPDATE_STRING);
        intent.putExtra(EmergencyResponses.CancelEmergency, settings.getBoolean(EmergencyResponses.CancelEmergency, true));
        intent.putExtra(ShockDetectService.AccelerationKeyString, settings.getFloat(ShockDetectService.AccelerationKeyString, 0.0f));
        intent.putExtra(TravellingService.TravellingKeyString, settings.getFloat(TravellingService.TravellingKeyString, 0.0f));
        broadcastManager.sendBroadcast(intent);

        if (intent.getFloatExtra(TravellingService.TravellingKeyString, 0.0f) >= TravellingService.tenKsPerHour &&
                intent.getFloatExtra(ShockDetectService.AccelerationKeyString, 0.0f) > 0.0f) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, speed, null);
            }
            Intent toSend = new Intent(EmergencyResponses.RECEIVE_EMERGENCY_NOTICE);
            toSend.putExtra(EmergencyResponses.CancelEmergency, false);
            LocalBroadcastManager.getInstance(this).sendBroadcast(toSend);
        }
    }
}
