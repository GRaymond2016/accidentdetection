package adet.riderdown;

import android.content.Intent;
import android.content.SharedPreferences;

/**
 * Created by lordnyson on 1/9/2017.
 */

public class ConfigurationCoding {
    public static final String RECEIVE_CONFIGURATION_OPTIONS = "ConfigureScreen.ConfigurationOptions";

    public static final String ConfAccelBeforeEmergencyKey = "ConfAccelBeforeEmergencyKey";
    public static final String ConfTimeBeforeCallKey = "ConfTimeBeforeCallKey";
    public static final String ConfEmergencyNumberKey = "ConfEmergencyNumberKey";

    public static final String ConfCallNumberKey = "ConfCallNumberKey";
    public static final String ConfAcceptCallsKey = "ConfAcceptCallsKey";
    public static final String ConfSendTextKey = "ConfSendTextKey";

    public static void encodeConfiguration(ConfigurationModel model, Intent target)
    {
        target.putExtra(ConfCallNumberKey, model.callNumber);
        target.putExtra(ConfAcceptCallsKey, model.acceptCalls);
        target.putExtra(ConfSendTextKey, model.sendText);
        target.putExtra(ConfAccelBeforeEmergencyKey, model.accelBeforeEmergency);
        target.putExtra(ConfTimeBeforeCallKey, model.timeBeforeCall);
        target.putExtra(ConfEmergencyNumberKey, model.emergencyNumber);
    }
    public static void decodeConfiguration(ConfigurationModel model, Intent target)
    {
        model.acceptCalls = target.getBooleanExtra(ConfAcceptCallsKey, true);
        model.sendText = target.getBooleanExtra(ConfSendTextKey, true);
        model.callNumber = target.getBooleanExtra(ConfCallNumberKey, true);
        model.accelBeforeEmergency = target.getIntExtra(ConfAccelBeforeEmergencyKey, 50);
        model.timeBeforeCall = target.getIntExtra(ConfTimeBeforeCallKey, 60);
        model.emergencyNumber = target.getStringExtra(ConfEmergencyNumberKey);
    }
    //These should probably be more generic but eh...
    public static void readConfiguration(ConfigurationModel model, SharedPreferences target)
    {
        model.callNumber = target.getBoolean(ConfCallNumberKey, true);
        model.acceptCalls = target.getBoolean(ConfAcceptCallsKey, true);
        model.sendText = target.getBoolean(ConfSendTextKey, true);
        try {
            model.accelBeforeEmergency = Integer.parseInt(target.getString(ConfAccelBeforeEmergencyKey, "50"));
        } catch (NumberFormatException ex) {
        }
        try {
            model.timeBeforeCall = Integer.parseInt(target.getString(ConfTimeBeforeCallKey, "60"));
        } catch (NumberFormatException ex) {
        }
        model.emergencyNumber = target.getString(ConfEmergencyNumberKey, "");
    }
    public static void writeConfiguration(ConfigurationModel model, SharedPreferences target)
    {
        SharedPreferences.Editor edit = target.edit();
        edit.putBoolean(ConfCallNumberKey, model.callNumber);
        edit.putBoolean(ConfAcceptCallsKey, model.acceptCalls);
        edit.putBoolean(ConfSendTextKey, model.sendText);
        edit.putString(ConfAccelBeforeEmergencyKey, model.accelBeforeEmergency.toString());
        edit.putString(ConfTimeBeforeCallKey, model.timeBeforeCall.toString());
        edit.putString(ConfEmergencyNumberKey, model.emergencyNumber);
        edit.commit();
    }
}
