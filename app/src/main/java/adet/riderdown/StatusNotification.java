package adet.riderdown;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.v4.app.NotificationCompat;

/**
 * Created by lordnyson on 1/24/2017.
 */

public class StatusNotification {
    public static final int NOTIFICATION_ID = 0xdf341;

    static void Create(Service service) {
        NotificationCompat.Builder not_build = new NotificationCompat.Builder(service);
        Context context = service.getApplicationContext();
        ApplicationInfo appInfo = context.getApplicationInfo();

        not_build.setSmallIcon(R.drawable.common_google_signin_btn_icon_dark);
        not_build.setContentTitle( appInfo.labelRes == 0 ? appInfo.nonLocalizedLabel : context.getString(appInfo.labelRes) + " Service");
        not_build.setContentText("Checking for collision events. Ride safe!");
        not_build.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        NotificationManager mNotificationManager = (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, not_build.build());
    }
}
