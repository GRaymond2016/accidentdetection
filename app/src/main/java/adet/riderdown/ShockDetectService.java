package adet.riderdown;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.net.ParseException;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;

import static adet.riderdown.ConfigurationCoding.ConfAccelBeforeEmergencyKey;
import static adet.riderdown.ConfigurationCoding.RECEIVE_CONFIGURATION_OPTIONS;

class ShockDetectService implements SensorEventListener {
    private double impulseToTrigger = 9.8 * 5;
    static final String RECEIVE_ACCELERATION_NOTIFICATION = "detectorservice.accelerationnotification";
    static final String AccelerationKeyString = "acceleration";
    private static final long POLL_INTERVAL = 1000;

    private float total_accel = 0;
    private long accel_count = 0;
    private long last_update = 0;
    private LocalBroadcastManager localBroadcast;

    private final BroadcastReceiver config_recv = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int accel_before_emergecy = intent.getIntExtra(ConfAccelBeforeEmergencyKey, -1);
            if (accel_before_emergecy == -1)
                return;
            try {
                impulseToTrigger = accel_before_emergecy;
            } catch (ParseException ex) {
            }
        }
    };

    ShockDetectService(LocalBroadcastManager localBroadcast, ConfigurationModel model) {
        this.localBroadcast = localBroadcast;

        impulseToTrigger = model.accelBeforeEmergency;
        this.localBroadcast.registerReceiver(config_recv, new IntentFilter(RECEIVE_CONFIGURATION_OPTIONS));
    }

    void destroy() {
        localBroadcast.unregisterReceiver(config_recv);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER)
            return;

        total_accel += Math.abs(event.values[0]) + Math.abs(event.values[1]) + Math.abs(event.values[2]);
        accel_count++;
        if (SystemClock.uptimeMillis() > POLL_INTERVAL + last_update) {
            float accell = total_accel / accel_count;
            if (accell > impulseToTrigger) {
                Intent intent = new Intent(RECEIVE_ACCELERATION_NOTIFICATION);
                intent.putExtra(AccelerationKeyString, accell);
                localBroadcast.sendBroadcast(intent);
            }
            total_accel = 0;
            accel_count = 0;
            last_update = SystemClock.uptimeMillis();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
