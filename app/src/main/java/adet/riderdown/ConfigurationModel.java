package adet.riderdown;

/**
 * Created by lordnyson on 1/9/2017.
 */

public class ConfigurationModel {
    public Integer accelBeforeEmergency;
    public Integer timeBeforeCall;
    public String emergencyNumber;
    public Boolean callNumber;
    public Boolean acceptCalls;
    public Boolean sendText;
}
